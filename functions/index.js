const functions = require('firebase-functions');
const utils = require('./utils');
const parse = require('./parse');

const express = require("express");
const exhbs = require("express-handlebars");
const app = express();

exports.detectAddition = utils.detectAddition;
exports.updatePatienceTime = utils.updatePatienceTime;
exports.getMissingProductDetails = utils.getMissingProductDetails;
const admin = utils.admin;

const path = require('path');
const public = path.join(__dirname, '../public');

//app.use(express.static(public));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/index", (req,res) => {
	res.sendFile(path.join(__dirname, '../public/index.html'));
})

app.get("/export-data/:uid", (req, res) => {

	const time1 = Date.now();
	admin.database().ref('/').once('value', data => {
		//get all the data
		console.log((Date.now() - time1)/1000 + "s to get data");

		const time2 = Date.now();
		const filename = "data1" + Date.now() + '.xlsx';
		const file = parse.json_to_xlsx({
			"user": parse.make_user_sheet(data),
			"sessions": parse.make_sessions_sheet(data),
			"product": parse.make_product_sheet(data),
			"product_status": parse.make_product_status_sheet(data),
			"date": parse.make_date_sheet(),
			"signup": parse.make_signuplog_sheet(data)
		});

		console.log((Date.now() - time2)/1000 + "s to make sheets");
		res.setHeader('Content-Disposition', `attachment; filename="${filename}";`);
		res.end(file);

	})
	
	// console.log(req.params.uid);

	// utils.isAdmin(req.params.uid).then(() => {
	// 	const time1 = Date.now();
	// 	admin.database().ref('/').once('value', data => {
	// 		//get all the data
	// 		console.log((Date.now() - time1)/1000 + "s to get data");
	
	// 		const time2 = Date.now();
	// 		const filename = "data1" + Date.now() + '.xlsx';
	// 		const file = parse.json_to_xlsx({
	// 			"user": parse.make_user_sheet(data),
	// 			"sessions": parse.make_sessions_sheet(data),
	// 			"product": parse.make_product_sheet(data),
	// 			"product_status": parse.make_product_status_sheet(data),
	// 			"date": parse.make_date_sheet()
	// 		});
	
	// 		console.log((Date.now() - time2)/1000 + "s to make sheets");
	// 		res.setHeader('Content-Disposition', `attachment; filename="${filename}";`);
	// 		res.end(file);
	
	// 	})
	// 	return true;
	// }).catch(err => {
	// 	console.log(err);
	// 	res.send("You aren't authorized to view this page")
	// 	return err;
	// })

});

app.get("/check-promise/:asin", (req, res) => {
	utils.getASINpage(req.params.asin, (data) => {
		res.send(data.data)
	});
});

app.get("/patience/:uid", (req, res) => {
	utils.getPatience(res, req.params.uid);
})

app.post("/patience/:uid", (req, res) => {
	utils.postPatience(req.body, req.params.uid, res);
})

exports.app = functions.https.onRequest(app);



















