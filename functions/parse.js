const XLSX = require("xlsx");
const clustering = require("density-clustering");
const optics = new clustering.OPTICS();

function referDict(array) {
	const dict = {
		"delay_time" : "delay_time_in_seconds",
		"total_time_spent" : "total_time_spent_in_seconds",
		"total_time" : "total_time_in_seconds"
	}

	const newArray = [];

	array.forEach(el => {
		if(dict[el]) {
			newArray.push(dict[el]);
		}	else	{
			newArray.push(el);
		}
	})

	return newArray;
}

exports.make_date_sheet = () => {
	return [
		["Created On", (new Date()).toString()]
	];
}

exports.make_signuplog_sheet = data => {
	const log = []

	const list = Object.keys(data.child('/signuptime/').val());

	list.forEach(l => {
		const record = {}
		const thisRef = data.child(`/signuptime/${l}`);

		record["uid"] = l;
		record["email"] = thisRef.child('email').exists() ? thisRef.child('email').val() : "not found";
		record["time"] = thisRef.child('time').exists() ? (new Date( thisRef.child('time').val() )).toString() : "not found";

		log.push(record);
		
	})

	return log;
}

exports.make_user_sheet = data => {
		const startTime = Date.now();
		const exp = [];

		const users = Object.keys(data.child('/users/').val());

		users.forEach(u => {
			const details = {};

			const thisUserRef = data.child(`/users/${u}`);

			details["userId"] = u;
			details["email"] = thisUserRef.child('/data/email').exists() ? thisUserRef.child('/data/email').val() : "not found";
			details["download_date_UTC"] = thisUserRef.child('/data/time').exists() ? thisUserRef.child('/data/time').val() : "not found";
			details["download_date_read"] = thisUserRef.child('/data/time').exists() ? (new Date(thisUserRef.child('/data/time').val())).toString() : "not found";
			details["delay_category"] = thisUserRef.child('/data/patienceCategory').exists() ? thisUserRef.child('/data/patienceCategory').val() : "not found";
			details["delay_time"] = data.child('/patience/').exists() ? (data.child('/patience/' + details["delay_category"] + '/time/').val())/1000 : "not found";
			details["num_times_patience_checked"] = 0;

			if(thisUserRef.child('/pages/').exists()) {
				let count = 0;
				let time = 0;
				let days = {};

				const pages = thisUserRef.child('/pages/').val();
				const pageNames = Object.keys(pages);

				pageNames.forEach(p => {
					//a check for the URL structure
					if(pages[p]["url"]) {
						count += 1;
					}

					//not all pages will have the timeSpent, these are the active pages
					time += pages[p]["timeSpent"] ? pages[p]["timeSpent"] : 0;

					//store only the date part
					const dateString = (new Date(pages[p]["time"])).toDateString();

					if(dateString !== "Invalid Date") {
						days[dateString] = true;
					}
				})

				details["page_count"] = count;
				details["total_time_spent"] = parseInt(time/1000); //Converting to seconds
				details["days_used"] = Object.keys(days).length;
			}

			if(thisUserRef.child('/product').exists()) {
				const products = thisUserRef.child('/product').val();
				const productIndex = Object.keys(products);
				let timeCheckCounter = 0;

				Object.keys(products).forEach(el => {
					Object.keys(products[el]).forEach(em => {
						if(products[el][em]["operation"] === "timeCheck") {
							timeCheckCounter += 1;
						}
					})
				});

				details["num_times_patience_checked"] = timeCheckCounter;
			}

			details["product_count"] = thisUserRef.child('/product/').exists() ? Object.keys(thisUserRef.child('/product/').val()).length : "not found";

			exp.push(details)
		});

		console.log("make_user_sheet took, ", (Date.now() - startTime)/1000)

		return exp;
}

exports.make_sessions_sheet = data => {
	const startTime = Date.now();

	const exp = [];

	const users = Object.keys(data.child('/users/').val());

	users ? users.forEach(userId => {
		const pages = data.child(`/users/${userId}/pages`).val();
		const simpleList = [];
		const dataset = [];

		pages && Object.keys(pages).forEach(pg => {
			if(typeof pages[pg].url !== 'undefined') {
				simpleList.push(pages[pg]);
				dataset.push([pages[pg].time, 0]); //clustering algorithm supports 2D.
			}
		});

		//pages around 15 seconds each can considered to be in same session
		const clusters = optics.run(dataset, 600000, 2);

		clusters.forEach(cl => {
			const list = [];
			let totalTime = 0;
			let prime = false;

			let lastTime = 0;
			let stopTime = 0;

			cl.forEach(t => {
				totalTime += simpleList[t].timeSpent || 0;

				//if any page in session is prime, the session is considered prime
				prime = prime || simpleList[t].prime;

				if(stopTime > dataset[t][0]) {
					stopTime = dataset[t][0];
					lastTime = simpleList[t].timeSpent;
				}

				list.push(dataset[t][0]);
			});

			const min = Math.min(...list);
			const max = Math.max(...list);

			exp.push({
				"user_id": userId,
				"start_time": (new Date(min)).toString(),
				"stop_time": (new Date(max + lastTime)).toString(),
				"total_time": (totalTime/1000),
				"num_pages": cl.length,
				"is_prime" : prime ? 'Y':'N'
			});
		})
		
	}) : null;

	console.log("make_sessions_sheet took, ", (Date.now() - startTime)/1000)
	return exp;
}

exports.make_product_sheet = data => {
	const startTime = Date.now();

	const exp = [];
	const addedIds = []
	const users = Object.keys(data.child('/users/').val());

	users.forEach(user => {
		const products = data.child(`/users/${user}/product`).val();
		const productInfo = data.child(`/users/${user}/product_data`).val();

		function addDetails(pr, act) {
			const userId = user;
				const productId = pr;
				const productName = productInfo[pr] ? productInfo[pr]["title"] : "not found";
				const productURL = productInfo[pr] ? productInfo[pr]["url"] : "not found";
				const productCategory = productInfo[pr] && productInfo[pr]["categories"] && productInfo[pr]["categories"].join(' > ');
				const isPrime = productInfo[pr] ? productInfo[pr]["isPrime"] : "not found";
				let expiration = "na";

				if(products[pr][act] && products[pr][act]["operation"] === "added" && products[pr][act]["patience"] && products[pr][act]["timestamp"]) {
					expiration = (new Date(products[pr][act]["patience"] + products[pr][act]["timestamp"])).toString();
				}

				exp.push({
					userId,
					productId,
					productName,
					productURL,
					productCategory,
					isPrime,
					expiration
				});

				addedIds.push(productId);
		}

		products ? Object.keys(products).forEach(pr => {
			Object.keys(products[pr]).forEach(act => {

				if(products[pr][act]["operation"] === "added") {
					addDetails(pr, act);
				}

			})
		}) : null;

		products ? Object.keys(products).forEach(pr => {
			Object.keys(products[pr]).forEach(act => {

				if(products[pr][act]["operation"] === "purchase" && addedIds.indexOf(pr) === -1) {
					addDetails(pr, act);
				}

			})
		}) : null;
	})
	console.log("make_product_sheet took, ", (Date.now() - startTime)/1000);
	return exp;
}

exports.make_product_status_sheet = data => {
	const startTime = Date.now();

	const exp = [];

	const users = Object.keys(data.child('/users/').val());

	users.forEach(user => {
		const products = data.child(`/users/${user}/product`).val();
		const productInfo = data.child(`/users/${user}/product_data`).val();

		const row = [];

		products ? Object.keys(products).forEach(pr => {
			Object.keys(products[pr]).forEach(act => {

				const userId = user;
				const productId = pr;
				const uniqueId = "-";
				const status = products[pr][act]["operation"];
				const timeStamp = products[pr][act]["timestamp"];
				const price = (!!productInfo[pr] && !!productInfo[pr]["price"]) ? productInfo[pr]["price"] : "not found";
				const quantity = (!!productInfo[pr] && !!productInfo[pr]["quantity"]) ? productInfo[pr]["quantity"] : "not found";

				row.push({
					userId,
					productId,
					uniqueId,
					status,
					timeStamp,
					price,
					quantity
				});

			})
		}) : null;

		//Tracks when the last purchase of an asin was made.
		//Helps to assign unique ids
		const lastPurchaseTrack = {};
		const addTrack = {}
		const uniqueRow = [];

		row.sort((a, b) => {return (a.timeStamp - b.timeStamp)});

		row.forEach(r => {
			if(Object.keys(lastPurchaseTrack).indexOf(r.productId) === -1) {
				lastPurchaseTrack[r.productId] = 1;
			}

			r["uniqueId"] = `${r.userId}_${r.productId}_${lastPurchaseTrack[r.productId]}`;

			if(r.status === "purchase" || r.status === "deleted") {
				lastPurchaseTrack[r.productId] = lastPurchaseTrack[r.productId] + 1;
				if(addTrack[r.productId]) {
					delete addTrack[r.productId];
				}
			}

			if(r.status === "added") {
				if (Object.keys(addTrack).indexOf(r.productId) === -1) {
					addTrack[r.productId] = 1;
				}	else {
					addTrack[r.productId] = addTrack[r.productId] + 1;
					r.status = "quantityUpdate";
				}
			}

			r.timeStamp = (new Date(r.timeStamp)).toString();

			uniqueRow.push(r);
		})

		exp.push(...uniqueRow);

	})
	console.log("make_product_status_sheet took, ", (Date.now() - startTime)/1000);
	return exp;
}

exports.json_to_xlsx = inf => {
	const startTime = Date.now();

	//Write the JSON in an excel file;
	//hoping that the first element has all the keys being used.

	//const allData = Array.isArray(inf) ? inf : [inf];

	const sheets = Object.keys(inf);

	const wb = XLSX.utils.book_new();

	function getSheet(exp) {

		if(exp.length === 0) {
			return false;
		}

		const superArray = [];
		const keys = Object.keys(exp[0]);

		//replace some of the key names to indicate units
		const keyNames = referDict(keys);

		superArray.push(keyNames);

		exp.forEach(u => {
			const details = [];

			keys.forEach(k => {
				details.push(u[k] || null);
			})

			superArray.push(details);
		});

		return XLSX.utils.aoa_to_sheet(superArray);

		//const wb = XLSX.utils.book_new();

		//const ws = XLSX.utils.aoa_to_sheet(superArray);
	}

	sheets.forEach((el, index) => {
		const sheet = getSheet(inf[el]);
		if(sheet) {
			XLSX.utils.book_append_sheet(wb, sheet, `sheet_${el}`);
		}
	});
	const xlsxWrite = XLSX.write(wb, {"type":"buffer", "bookType":"xlsx"});
	console.log("make_product_status_sheet took, ", (Date.now() - startTime)/1000);
	return xlsxWrite

}