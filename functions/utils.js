const functions = require('firebase-functions');
const admin = require('firebase-admin');
const axios = require('axios');

const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const adminEmails = require("./adminEmails").adminEmails();

admin.initializeApp();

const axiosBrowser = axios.create({
    'User-Agent': `Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36`,
    'user-agent': `Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36`
})

exports.detectAddition = functions.database.ref('/users/{uid}/pages')
	.onCreate((snapshot, context) => {
		return admin.database().ref('/patience/').once('value', data => {
			const categories = Object.keys(data.val());
            const activeCategories = categories.filter(key => {
                const value = data.val()[key]["active"]
                if(typeof value === 'boolean') {
                    return value;
                }   else {
                    return (value === 'true')
                }
            })
			const patienceCategory = activeCategories[Math.floor(Math.random()*activeCategories.length)];

			return admin.database().ref('/users/' + context.params.uid + '/data')
					.set({
						"picture" : context.auth.token.picture || null,
						"email" : context.auth.token.email || null,
						"name" : context.auth.token.name || context.auth.token.displayName || null,
						"time" : Date.now(),
						"consent": false,
						"patienceCategory": patienceCategory
					});
		})
	});

exports.updatePatienceTime = functions.database.ref('/users/{uid}/product/{asin}/{someId}')
	.onCreate((snapshot, context) => {
		if(snapshot.val().operation === "added") {
			console.log(`${context.params.asin} has been added.`);
			return admin.database().ref(`/users/${context.params.uid}/`).once('value', data => {
				const patienceCategory = data.val().data.patienceCategory;

				return admin.database().ref('/patience/').once('value', data => {

					let patience = data.val()[patienceCategory]["time"] || 0;
					let updates = {};
					updates[`/users/${context.params.uid}/product/${context.params.asin}/${context.params.someId}/patience`] = patience; 

					return admin.database().ref().update(updates);
				})

			})

		}	else {
			console.log(JSON.stringify(snapshot.val()));
			return 0
		}
	});

const getASINpage = function(asin, cb) {
    const url1 = `https://www.amazon.com/dp/${asin}`;
    const url2 = `https://www.amazon.com/gp/product/${asin}`;
    const url3 = `https://www.amazon.com/gp/product/glance/${asin}`;

    Promise.race([
            axiosBrowser.get(url1),
            axiosBrowser.get(url2),
            axiosBrowser.get(url3)
        ]).then( val => {
            cb(val);
            return true;
        }).catch(err => {
            console.log(err, "Leonidas at it again, envoys are down the well");
        });
}

exports.getASINpage = getASINpage;

exports.getMissingProductDetails = functions.database.ref('/users/{uid}/product/{asin}')
    .onCreate((snapshot, context) => {
        const prom = new Promise((resolve, reject) => {
            setTimeout(() => {
                admin.database().ref(`/users/${context.params.uid}/product_data/${context.params.asin}`).once('value', (snap, cont) => {
                if(snap.exists() && typeof snap.val().asin !== 'undefined') {
                    console.log("data exists");
                    resolve("data exists");
                }   else{
                    getASINpage(context.params.asin, html => {
                        console.log("got the page");
                        const receivedData = scrapeData(html.data, context.params.asin);
                        if(receivedData) {
                            admin.database()
                                .ref(`/users/${context.params.uid}/product_data/${context.params.asin}`)
                                .update(receivedData)
                                .then(() => {
                                    resolve("data scraped");
                                    return true;
                                })
                                .catch(err => {
                                    console.log(err);
                                    reject(new Error("scraping failed"));
                                })
                        }   else{
                            reject(new Error("something went wrong"))
                        }
                    })
                }
                })
            }, 5000);
        });

        return prom;
    });

exports.isAdmin = uid => {
    const check = new Promise((resolve, reject) => {
        if(uid) {
            const data = admin.database().ref(`/users/${uid}/data`).once('value')
            data.then((snapshot, context) => {
                if(snapshot.exists) {
                    const userData = snapshot.val();
                    if(userData.email) {
                        if(adminEmails.indexOf(userData.email) > -1) {
                            resolve()
                            return true;
                        }   else {
                            reject(new Error("Not an admin"));
                            return false;
                        }
                    }   else {
                        reject(new Error("Email for this not found"));
                        return false;
                    }
                }   else {
                    reject(new Error("User not found"));
                    return false;
                }
            })
            .catch(err => {
                reject(err)
            })
        }   else {
            reject(new Error("no uid found"));
        }
    })

    return check;
}

exports.getPatience = function(res, uid){

    let userData = null;
    let patienceData = null;

    const patiencePromise = new Promise(function(resolve, reject){
        admin.database().ref('/patience/').once('value')
        .then((snapshot, context) => {
            if(snapshot.exists()) {
                patienceData = snapshot.val();
                resolve();
            }   else{
                res.send("not found");
                reject(new Error("patience table not found"));
            }
            return true;
        }).catch(err => {
            res.send(err);
            reject(new Error("error getting the table"));
            return null;
        });
    })

    const userPromise = new Promise(function(resolve, reject) {
        admin.database().ref(`/users/${uid}/data`).once('value')
        .then((snapshot, context) => {
            console.log(snapshot.val())
            if(snapshot.exists()) {
                userData = snapshot.val();
                resolve();
            }   else    {
                res.send("not found")
                reject(new Error("user data not found"));
            }
            return true;
        }).catch(err => {
            res.send(err);
            reject(new Error("cannot find user data"))
            return null;
        });
    })

    Promise.all([userPromise, patiencePromise]).then(() => {
        if(adminEmails.indexOf(userData.email) > -1) {
            res.send(patienceData);
        }   else    {
            res.send("not found");
        }
        return true;
    }).catch(err => {
        console.log(err);
        return null;
    })

    return 0;
}

exports.postPatience = function(data, uid, res) {
    let userData = null;
    const userPromise = admin.database().ref(`/users/${uid}/data`).once('value');

    userPromise.then((snapshot, context) => {
        if(snapshot.exists()) {
            try {
                if(adminEmails.indexOf(snapshot.val().email) > -1) {
                    admin.database().ref('/patience').set(data)
                        .then(() => {
                            res.send("yes");
                            return true;
                        })
                        .catch(() => {
                            res.status(500).send("oops");
                            return null;
                        })
                }
            return true;
            } catch (err) {
                res.status(500).send("oops");
                return null;
            }
        }
        return true;
    }).catch(function(err) {
        console.log(err);
        return null;
    });
}


const scrapeData = function(html, asin_ordered){
        const document = (new JSDOM(html)).window.document;

        const getAsin = function(){
            let asin = null;
            asin = document.getElementById("ASIN") && document.getElementById("ASIN").value;

            return asin;
        };

        if(asin_ordered !== getAsin()) {
            console.log("asin mismatch")
            console.log(html);
            return false;
        }

        //is this a project page? Check if some elements are there
        if(!!document.getElementById("productTitle") ||
            !!document.getElementById("dp-container") ||
            !!document.getElementById("ppd")) {
            const asin = getAsin();
            let title = document.getElementById("productTitle") ||
                document.getElementById("title_feature_div");

                console.log(title.textContent);

            if(!title) {
                title = document.querySelector("meta[name='title']").content
                title = title.replace("Amazon.com: ","");
            }   else    {
                title = title.textContent.trim();
            }


            let price = document.getElementById("priceblock_ourprice") ||
                document.getElementById("priceblock_dealprice") ||
                document.getElementById("guild_priceblock_ourprice") ||
                document.getElementById("offer-price a-text-normal") ||
                document.querySelector("[id^='dmusic_digital_buy_button_buyBox']") ||
                document.querySelector("[name='displayedPrice']") ||
                document.querySelector(".guild_priceblock_value");

            if(price) {
                price = price.textContent || price.value;
                price = price.match(/([\d.])/g).join('');
                price = parseFloat(price);
            }   else {
                price = "not found";
            }


            const category_container = document.getElementById("wayfinding-breadcrumbs_container");
            const categories = [];
            const isPrime = !!(
                document.getElementById("prime_outer_div") ||
                document.getElementById("PrimeStripeContent") ||
                document.getElementsByClassName("primeUpsellIcon"));

            if(category_container) {
                const li = category_container.querySelectorAll("li");
                li.forEach(cat => {
                    if(!cat.classList.contains("a-breadcrumb-divider") && cat.id.indexOf("breadcrumb-back-link") === -1 && !cat.querySelector("#breadcrumb-back-link")) {
                        categories.push(cat.textContent.trim());
                    }
                })
            }

            const url = document.querySelector("link[rel='canonical']") ?
                        document.querySelector("link[rel='canonical']").getAttribute("href") : null;

            const info = {
                asin,
                title,
                price,
                isPrime,
                url,
                categories
            };

            console.log(info);

            return info;

            //chrome.runtime.sendMessage({message: "productFound", info: info});
        }
    };


exports.admin = admin;