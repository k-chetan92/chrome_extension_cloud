document.addEventListener('DOMContentLoaded', function() {
  // // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
  // // The Firebase SDK is initialized and available here!
  firebase.auth().onAuthStateChanged(user => {
      console.log(user) 
      if(user) {
        document.getElementById("hello").innerText = ('Hello! ' + user.displayName);
        var exportButton = document.querySelector("#message a")
        var uid = user.uid;
        var exportDataURL = exportButton.getAttribute("href") + "/" + uid;
        exportButton.setAttribute("href", exportDataURL);

        document.getElementById('sign-out').addEventListener('click', function() {
          firebase.auth().signOut();
        });

      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            try{
              var data = JSON.parse(this.response);
              var table = createTable(data);
              document.getElementById("dataTable").append(table.markup);
              document.getElementById("saveTable").style.display="block";
              document.getElementById("saveTable").addEventListener("click", function(){
                var updateData = table.fetchValues();

                var xhttpost = new XMLHttpRequest();
                xhttpost.open("POST", `https://us-central1-amazon-study-20181214.cloudfunctions.net/app/patience/${user.uid}`, true);
                xhttpost.setRequestHeader('Content-type','application/json; charset=utf-8');
                xhttpost.onload = function () {
                  if(xhttpost.readyState == 4 && xhttpost.status == 200) {
                    window.alert("data saved")
                  } else {
                    window.alert("something went wrong");
                  }
                }

                xhttpost.send(JSON.stringify(updateData));

              })
            } catch(err) {
              alert("You are not allowed to view this");  
            }
            
        }
      }

      xhttp.open("GET", `https://us-central1-amazon-study-20181214.cloudfunctions.net/app/patience/${user.uid}`, true);
      xhttp.send();

      } else {
        document.getElementById("hello").innerText = "Please sign in";
      }
    });

  // firebase.database().ref('/path/to/ref').on('value', snapshot => { });
  // firebase.messaging().requestPermission().then(() => { });
  // firebase.storage().ref('/path/to/ref').getDownloadURL().then(() => { });
  //
  // // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥

  function createTable(data, submitAction) {

    function createRow(key, value, active, control){
      var tr = document.createElement("tr");

      var td1 = document.createElement("td");
      var input1 = document.createElement("input");
      input1.setAttribute("type", "text");
      input1.setAttribute("key","")
      input1.setAttribute("value", key);
      td1.append(input1)

      tr.append(td1);

      var td2 = document.createElement("td");
      var input2 = document.createElement("input");
      input2.setAttribute("type", "text");
      input2.setAttribute("time","")
      input2.setAttribute("value", value);
      td2.append(input2)

      tr.append(td2);

      var td3 = document.createElement("td");
      var input3 = document.createElement("input");
      input3.setAttribute("type", "checkbox");
      input3.setAttribute("act","")
      input3.checked = active;
      td3.append(input3);

      tr.append(td3);

      var td4 = document.createElement("td");
      var input4 = document.createElement("input");
      input4.setAttribute("type", "checkbox");
      input4.setAttribute("ctrl","")
      input4.checked = control || false;
      td4.append(input4);

      tr.append(td4);

      

      return tr;
    }

    var fetchValues = function(){
      var result = {};
      var trs = markup.querySelectorAll("tr");
      
      for(var i=0, j=trs.length; i<j; i++) {
        var key = trs[i].querySelector("input[key]").value;
        var value = parseInt(trs[i].querySelector("input[time]").value);
        var active = trs[i].querySelector("input[act]").checked;
        var control = trs[i].querySelector("input[ctrl]").checked;

        if(key!== "") {
          result[key] = {
            time: value || 0,
            active: !!active,
            control: !!control
          }
        }

      }

      return result;
    }
    
    var keys = Object.keys(data);

    var markup = document.createElement("table");

    for(var i=0, j=keys.length; i<j; i++ ) {
      markup.append(createRow(keys[i],data[keys[i]]["time"],data[keys[i]]["active"], data[keys[i]]["control"]));
    }

    markup.append(createRow("","", true));

    

    return {markup, fetchValues};
  }

  var uiConfig = {
    signInSuccessUrl: '/',
    signInOptions: [
      // Leave the lines as is for the providers you want to offer your users.
      firebase.auth.GoogleAuthProvider.PROVIDER_ID
      //firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      //firebase.auth.TwitterAuthProvider.PROVIDER_ID,
      //firebase.auth.GithubAuthProvider.PROVIDER_ID,
      //firebase.auth.EmailAuthProvider.PROVIDER_ID,
      //firebase.auth.PhoneAuthProvider.PROVIDER_ID
    ],
    // Terms of service url.
    tosUrl: 'http://google.com'
  };

  var ui = new firebaseui.auth.AuthUI(firebase.auth());
  ui.start('#firebaseui-auth-container', uiConfig);

});